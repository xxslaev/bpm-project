import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        //IN_CONTRACT_SUM_1AR - массив сумм контракта. индекс = номер контракта, значение = сумма контракта.
        Long[] IN_CONTRACT_SUM_1AR = new Long[12];
        IN_CONTRACT_SUM_1AR[0] = 100000L;
        IN_CONTRACT_SUM_1AR[1] = 500L;
        IN_CONTRACT_SUM_1AR[2] = 2000L;
        IN_CONTRACT_SUM_1AR[3] = 2000000L;
        IN_CONTRACT_SUM_1AR[4] = 400L;
        IN_CONTRACT_SUM_1AR[5] = 6700L;
        IN_CONTRACT_SUM_1AR[6] = null;
        IN_CONTRACT_SUM_1AR[7] = 5000L;
        IN_CONTRACT_SUM_1AR[8] = 15000L;
        IN_CONTRACT_SUM_1AR[9] = 10L;
        IN_CONTRACT_SUM_1AR[10] = 0L;
        IN_CONTRACT_SUM_1AR[11] = 890000L;

        //IN_CONTRACT_CUR_1AR - массив кодов валют. индекс = номер контракта, значение = код валюты.
        String[] IN_CONTRACT_CUR_1AR = new String[12];
        IN_CONTRACT_CUR_1AR[0] = "USD";
        IN_CONTRACT_CUR_1AR[1] = "RUB";
        IN_CONTRACT_CUR_1AR[2] = "EUR";
        IN_CONTRACT_CUR_1AR[3] = "CNY";
        IN_CONTRACT_CUR_1AR[4] = "KZT";
        IN_CONTRACT_CUR_1AR[5] = "USD";
        IN_CONTRACT_CUR_1AR[6] = null;
        IN_CONTRACT_CUR_1AR[7] = "USD";
        IN_CONTRACT_CUR_1AR[8] = "EUR";
        IN_CONTRACT_CUR_1AR[9] = "";
        IN_CONTRACT_CUR_1AR[10] = "USD";
        IN_CONTRACT_CUR_1AR[11] = "RUB";

        //IN_CUR_CODE_1AR - массив кодов валют. индекс = id валюты, значение = код валюты.
        String[] IN_CUR_CODE_1AR = new String[3];
        IN_CUR_CODE_1AR[0] = "EUR";
        IN_CUR_CODE_1AR[1] = "USD";
        IN_CUR_CODE_1AR[2] = "RUB";

        //IN_CUR_VALUE_1AR - массив курсов валют. индекс = id валюты, значение = курс валюты.
        Long[] IN_CUR_VALUE_1AR = new Long[3];
        IN_CUR_VALUE_1AR[0] = 500L;
        IN_CUR_VALUE_1AR[1] = 430L;
        IN_CUR_VALUE_1AR[2] = 6L;

        //IN_CONTRACT_PAYMENT_SUM_2AR - двумерный массив платежей контракта. индекс1 = номер контракта, индекс2 = номер платежа, значение = платеж в тенге (KZT).
        Long[][] IN_CONTRACT_PAYMENT_SUM_2AR = new Long[12][];
        IN_CONTRACT_PAYMENT_SUM_2AR[0] = new Long[3];
        IN_CONTRACT_PAYMENT_SUM_2AR[0][0] = 1500L;
        IN_CONTRACT_PAYMENT_SUM_2AR[0][1] = 3000L;
        IN_CONTRACT_PAYMENT_SUM_2AR[0][2] = 300L;

        IN_CONTRACT_PAYMENT_SUM_2AR[1] = new Long[4];
        IN_CONTRACT_PAYMENT_SUM_2AR[1][0] = 3000L;
        IN_CONTRACT_PAYMENT_SUM_2AR[1][1] = 350L;
        IN_CONTRACT_PAYMENT_SUM_2AR[1][2] = null;
        IN_CONTRACT_PAYMENT_SUM_2AR[1][3] = 3600L;

        IN_CONTRACT_PAYMENT_SUM_2AR[2] = new Long[1];
        IN_CONTRACT_PAYMENT_SUM_2AR[2][0] = 3600L;

        IN_CONTRACT_PAYMENT_SUM_2AR[3] = new Long[2];
        IN_CONTRACT_PAYMENT_SUM_2AR[3][0] = 300L;
        IN_CONTRACT_PAYMENT_SUM_2AR[3][1] = 3600L;

        IN_CONTRACT_PAYMENT_SUM_2AR[4] = new Long[3];
        IN_CONTRACT_PAYMENT_SUM_2AR[4][0] = 3700L;
        IN_CONTRACT_PAYMENT_SUM_2AR[4][1] = null;
        IN_CONTRACT_PAYMENT_SUM_2AR[4][2] = 3800L;

        IN_CONTRACT_PAYMENT_SUM_2AR[5] = new Long[2];
        IN_CONTRACT_PAYMENT_SUM_2AR[5][0] = 3000L;
        IN_CONTRACT_PAYMENT_SUM_2AR[5][1] = 0L;

        IN_CONTRACT_PAYMENT_SUM_2AR[6] = new Long[1];
        IN_CONTRACT_PAYMENT_SUM_2AR[6][0] = null;

        IN_CONTRACT_PAYMENT_SUM_2AR[7] = new Long[1];
        IN_CONTRACT_PAYMENT_SUM_2AR[7][0] = 3300L;

        IN_CONTRACT_PAYMENT_SUM_2AR[8] = new Long[3];
        IN_CONTRACT_PAYMENT_SUM_2AR[8][0] = null;
        IN_CONTRACT_PAYMENT_SUM_2AR[8][1] = 300L;
        IN_CONTRACT_PAYMENT_SUM_2AR[8][2] = 3900L;

        IN_CONTRACT_PAYMENT_SUM_2AR[9] = new Long[2];
        IN_CONTRACT_PAYMENT_SUM_2AR[9][0] = null;
        IN_CONTRACT_PAYMENT_SUM_2AR[9][1] = 3000L;

        IN_CONTRACT_PAYMENT_SUM_2AR[10] = new Long[1];
        IN_CONTRACT_PAYMENT_SUM_2AR[10][0] = 3200L;

        IN_CONTRACT_PAYMENT_SUM_2AR[11] = new Long[4];
        IN_CONTRACT_PAYMENT_SUM_2AR[11][0] = 4700L;
        IN_CONTRACT_PAYMENT_SUM_2AR[11][1] = null;
        IN_CONTRACT_PAYMENT_SUM_2AR[11][2] = 300L;
        IN_CONTRACT_PAYMENT_SUM_2AR[11][3] = 70L;

        System.out.println("общее кол-во всех контрактов");

        int length = IN_CONTRACT_CUR_1AR.length + IN_CONTRACT_SUM_1AR.length + IN_CUR_CODE_1AR.length + IN_CUR_VALUE_1AR.length + IN_CONTRACT_PAYMENT_SUM_2AR.length;
        System.out.println(length);
        System.out.println();

        System.out.println("общая сумма всех контрактов в тенге");
        int sum = 0;
        for (Long a : IN_CONTRACT_SUM_1AR) {
            sum += a == null ? 0 : a;
        }

        for (Long[] a : IN_CONTRACT_PAYMENT_SUM_2AR) {
            for (Long b : a) {
                sum += b == null ? 0 : b;
            }
        }
        System.out.println(sum + " " + IN_CONTRACT_CUR_1AR[4]);
        System.out.println();

        System.out.println("количество всех контрактов, где сумма контракта больше 55 000 тенге");

        int contractSum = 0;
        for (int i = 0; i < IN_CONTRACT_SUM_1AR.length; i++) {
            if (IN_CONTRACT_SUM_1AR[i] != null && IN_CONTRACT_SUM_1AR[i] >= 55000) {
                contractSum++;
            }
        }

        System.out.println(contractSum);
        System.out.println();

        System.out.println("количество платежей на контракт");

        for (int i = 0; i < IN_CONTRACT_PAYMENT_SUM_2AR.length; i++) {
            int contractNum = 0;
            for (int a = 0; a < IN_CONTRACT_PAYMENT_SUM_2AR[i].length; a++) {
                if (IN_CONTRACT_PAYMENT_SUM_2AR[i] != null) {
                    contractNum++;

                }
            }
            System.out.println("№ " + i + " количество платежей: " + contractNum);
        }
        System.out.println();

        System.out.println("общая сумма всех платежей на контракт");
        for (int i = 0; i < IN_CONTRACT_PAYMENT_SUM_2AR.length; i++) {
            int contractNum = 0;
            for (int a = 0; a < IN_CONTRACT_PAYMENT_SUM_2AR[i].length; a++) {
                if (IN_CONTRACT_PAYMENT_SUM_2AR[i][a] != null) {
                    contractNum += IN_CONTRACT_PAYMENT_SUM_2AR[i][a];
                }
            }
            System.out.println("№ " + i + " общая сумма платежа: " + contractNum);
        }
        System.out.println();

        System.out.println("средняя сумма");

        int mid1 = 0;

        for (Long a : IN_CONTRACT_SUM_1AR) {
            mid1 += a == null ? 0 : a;
        }
        mid1 /= IN_CONTRACT_SUM_1AR.length;

        int mid2 = 0;
        for (Long a : IN_CUR_VALUE_1AR) {
            mid2 += a == null ? 0 : a;
        }
        mid2 /= IN_CUR_VALUE_1AR.length;

        int mid3 = 0;
        for (int i = 0; i < IN_CONTRACT_PAYMENT_SUM_2AR.length; i++) {
            for (int a = 0; a < IN_CONTRACT_PAYMENT_SUM_2AR[i].length; a++) {
                if (IN_CONTRACT_PAYMENT_SUM_2AR[i][a] != null) {
                    mid3 += IN_CONTRACT_PAYMENT_SUM_2AR[i][a];
                }
            }
        }
        mid3 /= IN_CONTRACT_PAYMENT_SUM_2AR.length;

        System.out.println((mid1+mid2+mid3)/3);
        System.out.println();

        System.out.println("Второй вариант средней суммы");
        int mid4 = 0;
        int count = 0;
        for (int i = 0; i<IN_CONTRACT_SUM_1AR.length; i++) {
            if (IN_CONTRACT_SUM_1AR[i]!=null&&IN_CONTRACT_SUM_1AR[i]>150000){
                mid4+=IN_CONTRACT_SUM_1AR[i];
                count++;
            }
        }
        mid4 /= count;
        System.out.println(mid4);
        System.out.println();

    }
}
